
/**
 * HelloRequest
 * @targetNSAlias `tns`
 * @targetNamespace `http://learnwebservices.com/services/hello`
 */
export interface HelloRequest {
    /** xs:string */
    Name?: string;
}
