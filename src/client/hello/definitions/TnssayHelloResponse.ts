import { HelloResponse } from "./HelloResponse";

/** tns:SayHelloResponse */
export interface TnssayHelloResponse {
    /** HelloResponse */
    HelloResponse?: HelloResponse;
}
