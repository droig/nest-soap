import { HelloRequest } from "./HelloRequest";

/** tns:SayHello */
export interface TnssayHello {
    /** HelloRequest */
    HelloRequest?: HelloRequest;
}
