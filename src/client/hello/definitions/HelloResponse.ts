
/**
 * HelloResponse
 * @targetNSAlias `tns`
 * @targetNamespace `http://learnwebservices.com/services/hello`
 */
export interface HelloResponse {
    /** xs:string */
    Message?: string;
}
