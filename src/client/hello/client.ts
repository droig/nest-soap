import { Client as SoapClient, createClientAsync as soapCreateClientAsync } from "soap";
import { TnssayHello } from "./definitions/TnssayHello";
import { TnssayHelloResponse } from "./definitions/TnssayHelloResponse";
import { HelloEndpointService } from "./services/HelloEndpointService";

export interface HelloClient extends SoapClient {
    HelloEndpointService: HelloEndpointService;
    SayHelloAsync(sayHello: TnssayHello): Promise<[result: TnssayHelloResponse, rawResponse: any, soapHeader: any, rawRequest: any]>;
}

/** Create HelloClient */
export function createClientAsync(...args: Parameters<typeof soapCreateClientAsync>): Promise<HelloClient> {
    return soapCreateClientAsync(args[0], args[1], args[2]) as any;
}
