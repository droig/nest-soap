import { TnssayHello } from "../definitions/TnssayHello";
import { TnssayHelloResponse } from "../definitions/TnssayHelloResponse";

export interface HelloEndpointPort {
    SayHello(sayHello: TnssayHello, callback: (err: any, result: TnssayHelloResponse, rawResponse: any, soapHeader: any, rawRequest: any) => void): void;
}
