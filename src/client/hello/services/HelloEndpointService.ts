import { HelloEndpointPort } from "../ports/HelloEndpointPort";

export interface HelloEndpointService {
    readonly HelloEndpointPort: HelloEndpointPort;
}
