import { Injectable } from '@nestjs/common';
import * as path from 'path';
import { createClientAsync, HelloClient } from './client/hello';

@Injectable()
export class AppService {
  client: HelloClient;

  constructor() {
    createClientAsync(path.resolve('./wsdl/hello.wsdl')).then((client) => {
      this.client = client;
    });
  }

  async getHello() {
    try {
      const res = await this.client.SayHelloAsync({
        HelloRequest: { Name: 'Homero' },
      });
      return res[0].HelloResponse.Message;
    } catch (err) {
      console.error(err);
      throw new Error('Error de servicio');
    }
  }
}
