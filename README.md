# Prueba de consumo SOAP desde NestJS

Para este ejemplo se utilizó la herramienta 'wsdl-tsclient' que se puede instalar desde NPM.

```
$ npm i -g wsdl-tsclient
```

Luego se generó el cliente:
```
$ wsdl-tsclient wsdl/hello.wsdl -o ./src/client
```

